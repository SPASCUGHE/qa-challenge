Feature: Crew Application

    Requirements:
    - correctly defined test cases in BDD format
    - readiness for CI
    - edge cases are covered by tests
    - correct error messages
    - documentation: README and inline code comments
    - use of docker

    Background:
        Given I navigate to application homepage

    Scenario: As a user I can see the homepage elements
        Then I should see the application title and logo in header section
        And I should see the filter section
        And I should see the crew members board with applied, interviewing and hired statuses

    Scenario: As a user I can see crew member info
        Then I should see the crew member name, city and photo

    Scenario: As a user I can filter crew members by name
        When I type "emma" in name filter input
        And I select Submit button
        Then I should see the filtered name "emma stewart" on the board

    Scenario: As a user I can filter crew members by city
        When I type "liverpool" in city filter input
        And I select Submit button
        Then I should see the filtered city "liverpool" on the board

    Scenario: As a user I cannot filter by city in name filter
        When I type "liverpool" in name filter input
        And I select Submit button
        Then I should see empty crew members board

    Scenario: As a user I cannot filter by name in city filter
        When I type "danielle" in city filter input
        And I select Submit button
        Then I should see empty crew members board

    Scenario: As a user I cannot filter crew members by full name
        When I type "danielle moore" in name filter input
        And I select Submit button
        Then I should see empty crew members board

    Scenario: As a user I can see empty board when I filter by something that does not exists
        When I type "no name" in name filter input
        And I select Submit button
        Then I should see empty crew members board

    Scenario: As a user I can clear the applied filter
        When I type "nothing" in name filter input
        And I select Submit button
        Then I should see empty crew members board
        When I select Clear button
        Then I should see no filters applied on crew members board

    Scenario: As a user I can move a member from applied to interviewing state
        When I type "linda" in name filter input
        And I select Submit button
        And I move "linda ruiz" up from applied state to interviewing state
        Then I should see "linda ruiz" in interviewing state

    #This test will fail due to I put a wrong expected result
     Scenario: As a user I can move a member from interviewing to hired state
        When I type "linda" in name filter input
        And I select Submit button
        And I move "linda ruiz" up from interviewing state to hired state
        Then I should see "linda ruiz" in applied state

    Scenario: As a user I can move a member from hired to applied state
        When I type "julia" in name filter input
        And I select Submit button
        And I move "julia cunningham" down from hired state to applied state
        Then I should see "julia cunningham" in applied state


