import { Given, Then, When } from "cypress-cucumber-preprocessor/steps";
import crewApplication from "../../page-objects/crew-application";


Given(/^I navigate to application homepage$/, function () {
    crewApplication.navigateToBaseUrl();
});

When(/^I type "([^"]*)" in name filter input$/, function (membersName) {
    crewApplication.typeInNameFilter(membersName);
});

When(/^I type "([^"]*)" in city filter input$/, function (city) {
    crewApplication.typeInCityFilter(city);
});

When(/^I select (Submit|Clear) button$/, function (button) {
    if (button === 'Submit') {
        crewApplication.selectSubmit();
    } else {
        crewApplication.selectClear();
    }

});

When(/^I move "([^"]*)" up from applied state to interviewing state$/, function (name) {
    crewApplication.makesureMemberIsInColumn('Applied', name);
    crewApplication.moveCrewMember(name).contains('>').click();
});

When(/^I move "([^"]*)" up from interviewing state to hired state$/, function (name) {
    crewApplication.makesureMemberIsInColumn('Applied', name);
    crewApplication.moveCrewMember(name).contains('>').click();
    crewApplication.makesureMemberIsInColumn('Interviewing', name);
    crewApplication.moveCrewMember(name).contains('>').click();
});

When(/^I move "([^"]*)" down from hired state to applied state$/, function (name) {
    crewApplication.makesureMemberIsInColumn('Hired', name);
    crewApplication.moveCrewMember(name).contains('<').click();
    crewApplication.makesureMemberIsInColumn('Interviewing', name);
    crewApplication.moveCrewMember(name).contains('<').click();
});

Then(/^I should see the application title and logo in header section$/, function () {
    crewApplication.verifyHeaderElements();
});

Then(/^I should see the filter section$/, function () {
    crewApplication.verifyFilterElements();
});

Then(/^I should see the crew members board with applied, interviewing and hired statuses$/, function () {
    crewApplication.verifyCrewMembersBoard();
});

Then(/^I should see the filtered name "([^"]*)" on the board$/, function (membersName) {
    crewApplication.verifyFilterByNameResult(membersName);
});

Then(/^I should see the filtered city "([^"]*)" on the board$/, function (city) {
    crewApplication.verifyFilterByCityResult(city);
});

Then(/^I should see empty crew members board$/, function () {
    crewApplication.verifyEmptyBoard();
});

Then(/^I should see no filters applied on crew members board$/, function () {
    crewApplication.verifyNoFilteredBoard();
});

Then(/^I should see the crew member name, city and photo$/, function () {
    crewApplication.verifyCrewMemberInfo();
});

Then(/^I should see "([^"]*)" in interviewing state$/, function (memberName) {
    crewApplication.makesureMemberIsInColumn('Interviewing', memberName);
});

Then(/^I should see "([^"]*)" in hired state$/, function (memberName) {
    crewApplication.makesureMemberIsInColumn('Hired', memberName);
});

Then(/^I should see "([^"]*)" in applied state$/, function (memberName) {
    crewApplication.makesureMemberIsInColumn('Applied', memberName);
});