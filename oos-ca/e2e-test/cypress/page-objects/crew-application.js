class CrewApplication {

    //section to declare the selectors

    get header() { return cy.get('.App-header'); }
    get logo() { return cy.get('.App-logo'); }
    get title() { return cy.get('.App-title'); }
    get filterSection() { return cy.get('#filters'); }
    get nameFilterInput() { return cy.get('#name'); }
    get cityFilterInput() { return cy.get('#city'); }
    get submitBtn() { return cy.get('form button').contains('Submit'); }
    get clearBtn() { return cy.get('form button').contains('Clear'); }
    get crewMembersBoard() { return cy.get('.App-container'); }
    get crewBoardAppliedColumn() { return cy.get('h2').contains('Applied'); }
    get crewBoardInterviewingColumn() { return cy.get('h2').contains('Interviewing'); }
    get crewBoardHiredColumn() { return cy.get('h2').contains('Hired'); }
    get crewMemberName() { return cy.get('.CrewMemeber-name > div:nth-child(1)'); }
    get crewMemberCity() { return cy.get('.CrewMemeber-name > div:nth-child(2)'); }
    get crewMemberCard() { return cy.get('CrewMember-container'); }
    get crewMemberPhoto() { return cy.get('.CrewMemeber-photo'); }

    //xpath selectors in order to be able to select a specific button of a specific member (maybe it can be improved in order to do not use xpath)
    get crewMemberBtnByName() { return (value) => cy.xpath(`//div[@class="CrewMemeber-name"]/div[text()=\'${value}\']/ancestor::div[@class="CrewMember-container"]//button`); }
    get columnOfMembers() { return (column, member) => cy.xpath(`//h2[text()=\'${column}\']/ancestor::div[@class="App-column"]//div[text()=\'${member}\']`); }


    //method for loading the homepage, baseUrl defined in config file
    navigateToBaseUrl() {
        cy.visit('/');
    }

    //method the check few UI elements from the application header
    verifyHeaderElements() {
        const titleText = 'OpenOceanStudio: Crew Applications';
        this.header.should('be.visible');
        this.logo.should('be.visible');
        this.title.should('be.visible').and('have.text', titleText);
    }

    //method to verify the filter secetion elements
    verifyFilterElements() {
        this.filterSection.should('be.visible');
        this.nameFilterInput.should('be.visible');
        this.cityFilterInput.should('be.visible');
        this.submitBtn.should('be.visible');
        this.clearBtn.should('be.visible');
    }

    //method to verify the crew Memebers Board elements
    verifyCrewMembersBoard() {
        this.crewMembersBoard.should('be.visible');
        this.crewBoardAppliedColumn.should('be.visible');
        this.crewBoardInterviewingColumn.should('be.visible');
        this.crewBoardHiredColumn.should('be.visible');
    }

    typeInNameFilter(membersName) {
        return this.nameFilterInput.type(membersName);
    }

    typeInCityFilter(city) {
        return this.cityFilterInput.type(city);
    }

    selectSubmit() {
        this.submitBtn.click();
    }

    selectClear() {
        this.clearBtn.click();
    }

    //method to verify filter results
    verifyFilterByNameResult(expectedResult) {
        this.crewMemberName.should('contain', expectedResult);
    }

    verifyFilterByCityResult(expectedResult) {
        this.crewMemberCity.should('contain', expectedResult);
    }

    //verify that no crew memebrs card exists on DOM
    verifyEmptyBoard() {
        this.crewMemberCard.should('not.exist');
    }

    verifyNoFilteredBoard() {
        this.crewMemberName.should('be.visible');
    }

    verifyCrewMemberInfo() {
        this.crewMemberPhoto.should('be.visible');
        this.crewMemberName.should('be.visible');
        this.crewMemberCity.should('be.visible');
    }

    makesureMemberIsInColumn(columnName, memberName) {
        return this.columnOfMembers(columnName, memberName).should('be.visible');
    }

    moveCrewMember(memberName) {
        return this.crewMemberBtnByName(memberName);
    }

}

export default new CrewApplication();