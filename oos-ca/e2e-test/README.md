# qa-challenge

## Installation

Clone the repository and run `npm i` in repository root.

## Running tests

```sh
npm run e2e
```

## Running cypress test runner
```sh
npx cypress open
```

#### Allure reporting part

```sh
npm run report
```
In case of allure report does not working locally, right click on index.html from allure-report directory, and select "Open with live server"

*A failed scenario is placed between test cases

![](Allure-report.png)
![](Allure-failure.png)
